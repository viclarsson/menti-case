import React, { Component } from 'react';
import PropTypes from 'prop-types';

import loader from '../../images/loading.svg';

export default class NearbySearch extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: null
        };

        this._renderContent = this._renderContent.bind(this);
    }

    _search() {
        this.setState({
            loading: true
        }, () => {
            setTimeout(() => {
                this.setState({
                    loading: false,
                    data: []
                });
            }, 2500);
        });
    }

    _renderContent() {
        if(this.state.loading) {
            return (
                <img className="loader loader--size-sm" src={loader} alt="Loading..." />
            );
        } else if(this.state.data) {
            return "No presentations found...";
        }
        return (
            <button className="button button--primary button--border button--size-sm" onClick={this._search.bind(this)}>Search</button>
        );
    }

    render() {
        return (
            <section className="grid grid--padding-md grid--bg-color-gray">
                <h3 className="t--align-center">Nearby presentations</h3>
                <p className="t--align-center t--size-sm u--height-fixed-md">
                    {this._renderContent()}
                </p>
            </section>
        );
    }
}
