import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class AccessCodeInput extends Component {

    constructor(props) {
        super(props);
        this.state = {
            first: "",
            second: "",
            third: ""
        };
    }

    _handleSubmit(form) {
        let code = this.refs.first.value + this.refs.second.value + this.refs.third.value;
        this.setState({
            first: "",
            second: "",
            third: ""
        });
        this.props.callback(code);
    }

    _handleChange(e) {
        if(e.target === this.refs.first) {
            this.setState({
                first: e.target.value.substr(0,2)
            });
            if(e.target.value.length >= 2) {
                this.refs.second.focus();
            }
        }
        if(e.target === this.refs.second) {
            this.setState({
                second: e.target.value.substr(0,2)
            });
            if(e.target.value.length >= 2) {
                this.refs.third.focus();
            } else if(e.target.value.length === 0) {
                this.refs.first.focus();
            }
        }
        if(e.target === this.refs.third) {
            this.setState({
                third: e.target.value.substr(0,2)
            });
            if(e.target.value.length === 0) {
                this.refs.second.focus();
            } else if(e.target.value.length == 2) {
                // Submit
                e.target.blur();
                this._handleSubmit();
            }
        }
    }

    render() {
        return (
            <form ref="codeform" className="code-input grid grid--direction-row">
                <input className="input input--size-lg" type="tel" placeholder="12" value={this.state.first} ref="first" onChange={this._handleChange.bind(this)} />
                <input className="input input--size-lg" type="tel" placeholder="34" value={this.state.second} ref="second" onChange={this._handleChange.bind(this)} />
                <input className="input input--size-lg" type="tel" placeholder="56" value={this.state.third} ref="third" onChange={this._handleChange.bind(this)} />
                <input type="submit" hidden/>
            </form>
        );
    }
}

AccessCodeInput.propTypes = {
    callback: PropTypes.func.isRequired
};
