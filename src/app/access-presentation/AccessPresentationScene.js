import React, { Component } from 'react';
import AccessCodeInput from './AccessCodeInput.js';
import NearbySearch from './NearbySearch.js';
import less from './access-presentation.less';
import mentimeter from '../../images/mentimeter.svg';

import * as UI from 'mentimeter-design/ui';
console.log(UI.Animated);

export default class AccessPresentationScene extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: false
    };
  }

  _mockLoading(code) {
    this.setState({
      loading: true,
      error: false
    }, () => {
      setTimeout(() => {
        this.setState({
          loading: false,
          error: true
        });
      }, 3000);
    });
  }

  render() {
    var loading = this.state.loading ? 'state-loading' : '';
    var error = this.state.error ? 'state-error' : '';
    var description = "Please enter the code";
    if(loading) description = "Checking...";
    if(error) description = "We couldn't find a presentation with that code... Try again!";
    return (
      <div id="access-presentation" className="grid grid--direction-column">
        <div className="mm-ui">
          <UI.Animated appear animation="slide-in">
            <UI.Button>Test</UI.Button>
          </UI.Animated>
        </div>
        <section id="code-input" className="grid grid--weight-1 grid--padding-lg grid--no-padding-horizontal grid--bg-color-blue grid--direction-column grid--image-meeting">
          <img src={mentimeter} alt="Mentimeter Logo" className="animated fadeInDown"/>
          <div id="access-presentation-content" className="grid grid--h-padding-sm grid--stretch grid--align-center grid--direction-column">
            <h6 className="t t--align-center t--color-white">Make your voice heard</h6>
            <h1 className="h2 t t--align-center t--color-white">
              {description}
            </h1>
            <div className={`${loading} ${error} p`} id="code-input-container">
              <AccessCodeInput callback={this._mockLoading.bind(this)} code={this.state.code}/>
            </div>
            <p className="t--style-meta">The code is in front of you.</p>
          </div>
        </section>
        <div id="presentation-location-search">
          <NearbySearch />
          <section className="grid grid--padding-sm grid--no-padding-top grid--bg-color-gray">
            <p className="t--align-center t--size-sm t--color-gray">Powered by <a href="https://www.mentimeter.com">mentimeter.com</a></p>
          </section>
        </div>
      </div>
    );
  }
}
