import React, { Component } from 'react';
import { render } from 'react-dom';

// Ignored router for now.
import { BrowserRouter, Route, Link, Switch } from 'react-router-dom';

// Basic less
import less from './less/build.less';

// Scenes
import AccessPresentationScene from './app/access-presentation/AccessPresentationScene.js';

// App
export default class App extends Component {
    render() {
        return (
            <AccessPresentationScene />
        );
    }
}


render(<App />, document.getElementById('root'));
