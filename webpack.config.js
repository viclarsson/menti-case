/*
*   Webpack config.
*   Uses conditional statements for prod/dev.
*   Should use different files?
*/

// Includes and plugin co
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const extractLess = new ExtractTextPlugin({
    filename: "[name].css",
    disable: isDevelopment
});

// Paths
var appSrc = path.resolve(__dirname, 'src/');
var buildSrc = path.resolve(__dirname, 'build/');
var port = 8080;

// Environment
var isDevelopment = process.env.NODE_ENV === "development";

// Entry
var entry = [
    appSrc + '/index.js'
];
if(isDevelopment) {
    entry.push('webpack-dev-server/client?http://localhost:8080');
}

/*
*   Config
*/
var config = {
    entry: entry,
    output: {
        path: buildSrc,
        filename: 'bundle.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        extractLess
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015', 'react'],
                        cacheDirectory: true,
                    },
                }
            },
            {
                test: /\.(jpe?g|png|gif)$/,
                loader: 'url-loader'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url-loader?limit=1000000&mimetype=image/svg+xml"
            },
            {
                test: /\.less$/,
                use: extractLess.extract({
                    use: [
                        {
                            loader: "css-loader"
                        },
                        'postcss-loader',
                        {
                            loader: "less-loader", options: {
                                paths: [
                                    path.resolve(__dirname, "src/less")
                                ]
                            }
                        },
                    ],
                    fallback: "style-loader"
                })
            }
        ]
    }
};

module.exports = config;
