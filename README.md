# Mentimeter Case
This is a case made for Mentimeter.com. The objective was to implement their new design into an existing view.

React was used due to "damma av det lite" using LESS as CSS compiler. This was also the first time using Webpack as bundler for both js, jsx, images and LESS.

## Case
The use-case presented in this work is when an user wants to access a presentation using their phone. The assumption is that the user goal is to access a presentation, which is focused by as easily as possible adding the presentation code, split into a 6-digit format. Also, the nearby presentations are shown as secondary action.

In this case, it will go badly. I personally believe that this is *very, very important* to design for. Do not only design for sunshine cases as people are usually happy when things work!

Also, I hate the hijack of 44 pixels... https://benfrain.com/the-ios-safari-menu-bar-is-hostile-to-web-apps-discuss/

#### Focus
My focus was to build a small and simple foundation for a modular code base, both for LESS and Js. Using this approach, it allows for designing in browser for easier tasks and a *"parametric design"* which keeps dimensions, spacings, colors and such limited to create a strong and consistent look.

## Running and building
Webpack is used and configured for development and building for production.

####Running development
`npm run dev`

####Build for production
`npm run build`
